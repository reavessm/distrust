# DisTRUST

Distributed system Test Runner Using Scalable Technology (DisTRUST) is a
framework for creating test cases and attempting to satisfy them.

## Description

This project creates a Kubernetes Operator to aid in the process of testing
students in a Distributed Systems course.  Professors will create multiple test
cases, and students will provide their container images to be tested against
those cases.  The reconciler loop will run the professor's input on the
student's containers, and verify the output.

### Examples

```yaml
---
apiVersion: distrust.reaves.dev/v1alpha1
kind: TestCase
metadata:
  name: basic
  namespace: homelab-distrust
  labels:
    dev.reaves.distrust/suite: basic
spec:
  case-id: basic
  client-inputs:
    - name: single
      inputs:
        - put foo bar
        - get foo
  server-outputs:
    - outputs:
        - bar
---
apiVersion: distrust.reaves.dev/v1alpha1
kind: Submission
metadata:
  name: basic
  namespace: homelab-distrust
  labels:
    dev.reaves.distrust/suite: basic
spec:
  client-image: 'quay.io/reavessm/distrust:client'
  server-image: 'quay.io/reavessm/distrust:server'
  username: 'sreaves31'
  server-port: 8080
```

See more examples in the `examples/demo` folder

## Demo

https://youtu.be/R4Ltbgr6TgA

## Getting Started

### Prerequisites
- go version v1.20.0+
- docker version 17.03+.
- kubectl version v1.11.3+.
- Access to a Kubernetes v1.11.3+ cluster.

### To Deploy on the cluster
**Build and push your image to the location specified by `IMG`:**

```sh
make docker-build docker-push IMG=<some-registry>/distrust:tag
```

**NOTE:** This image ought to be published in the personal registry you specified. 
And it is required to have access to pull the image from the working environment. 
Make sure you have the proper permission to the registry if the above commands don’t work.

**Install the CRDs into the cluster:**

```sh
make install
```

**Deploy the Manager to the cluster with the image specified by `IMG`:**

```sh
make deploy IMG=<some-registry>/distrust:tag
```

> **NOTE**: If you encounter RBAC errors, you may need to grant yourself cluster-admin 
privileges or be logged in as admin.

**Create instances of your solution**
You can apply the samples (examples) from the config/sample:

```sh
kubectl apply -k config/samples/
```

>**NOTE**: Ensure that the samples has default values to test it out.

### To Uninstall
**Delete the instances (CRs) from the cluster:**

```sh
kubectl delete -k config/samples/
```

**Delete the APIs(CRDs) from the cluster:**

```sh
make uninstall
```

**UnDeploy the controller from the cluster:**

```sh
make undeploy
```

## Contributing
// TODO(user): Add detailed information on how you would like others to contribute to this project

**NOTE:** Run `make help` for more information on all potential `make` targets

More information can be found via the [Kubebuilder Documentation](https://book.kubebuilder.io/introduction.html)

## Authors

Stephen Reaves

## License

Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

