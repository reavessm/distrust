/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// SubmissionSpec defines the desired state of Submission
type SubmissionSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// +kubebuilder:example="quay.io/my-repo/projectOne:client"
	// ClientImage is the container image to pull and use for the client jobs
	ClientImage string `json:"client-image"`

	// +kubebuilder:example="quay.io/my-repo/projectOne:server"
	// ServerImage is the container image to pull and use for the server jobs
	ServerImage string `json:"server-image"`

	// ServerPort is the port that the server will use
	ServerPort int32 `json:"server-port"`

	// UserName is the name of the person getting the grade
	UserName string `json:"username"`
}

type Result struct {
	Case string `json:"case"`

	// +kubebuilder:validation:Enum:=pending;running;passed;failed
	Status string `json:"status"`

	Diff string `json:"diff,omitempty"`
}

// SubmissionStatus defines the observed state of Submission
type SubmissionStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Results is an array describing which test cases passed/failed
	Results []Result `json:"results,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// Submission is the Schema for the submissions API
type Submission struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   SubmissionSpec   `json:"spec,omitempty"`
	Status SubmissionStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// SubmissionList contains a list of Submission
type SubmissionList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Submission `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Submission{}, &SubmissionList{})
}
