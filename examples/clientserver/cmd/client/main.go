package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	serverHost := os.Getenv("SERVER_HOST")
	if serverHost == "" {
		serverHost = "localhost"
	}

	serverPort := os.Getenv("SERVER_PORT")
	if serverPort == "" {
		serverPort = "8080"
	}

	serverURL := serverHost + ":" + serverPort
	fmt.Println(serverURL)

	for _, line := range os.Args {
		fmt.Println(line)
		tokens := strings.Split(line, " ")

		resp, err := http.Post("http://"+serverURL+"/"+tokens[0], "text/plain", strings.NewReader(strings.Join(tokens[1:], " ")))
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(resp)
	}
}
