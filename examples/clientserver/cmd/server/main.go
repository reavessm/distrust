package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

func main() {
	mux := http.NewServeMux()

	data := make(map[string]string)

	mux.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		bytes, err := io.ReadAll(r.Body)
		if err != nil {
			log.Println(err)
		}
		f := bufio.NewWriter(os.Stdout)
		defer f.Flush()
		f.Write([]byte(data[string(bytes)]))
		// fmt.Println(data[string(bytes)])
		fmt.Fprintf(w, "%s", bytes)
	})

	mux.HandleFunc("/put", func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		bytes, err := io.ReadAll(r.Body)
		if err != nil {
			log.Println(err)
		}
		s := strings.Split(string(bytes), " ")
		data[s[0]] = strings.Join(s[1:], " ")
	})

	http.ListenAndServe("0.0.0.0:8080", mux)
}
