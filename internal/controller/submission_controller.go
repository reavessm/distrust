/*
Copyright 2024.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/google/go-cmp/cmp"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/record"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/log"

	distrustv1alpha1 "gitlab.com/reavessm/distrust/api/v1alpha1"
)

// SubmissionReconciler reconciles a Submission object
type SubmissionReconciler struct {
	client.Client
	Scheme   *runtime.Scheme
	Recorder record.EventRecorder
}

//+kubebuilder:rbac:groups=distrust.reaves.dev,resources=submissions,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=distrust.reaves.dev,resources=submissions/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=distrust.reaves.dev,resources=submissions/finalizers,verbs=update
//+kubebuilder:rbac:groups=distrust.reaves.dev,resources=testcase,verbs=get;list;watch
//+kubebuilder:rbac:groups=apps,resources=statefulset,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=*,resources=pods,verbs=get;list;watch

const (
	labelKey           string = "dev.reaves.distrust"
	labelSuiteKey      string = labelKey + "/suite"
	labelSubmissionKey string = labelKey + "/submissionID"
	labelProject       string = "app.kubernetes.io/part-of"
	labelConnects      string = "app.openshift.io/connects-to"
)

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Submission object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.16.3/pkg/reconcile
func (r *SubmissionReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)

	logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", req))

	// TODO(user): your logic here
	submission := distrustv1alpha1.Submission{}
	if err := r.Get(ctx, req.NamespacedName, &submission); err != nil {
		if errors.IsNotFound(err) {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: Deleted %v", req.NamespacedName))
			return ctrl.Result{}, nil
		}
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
		return ctrl.Result{}, err
	}

	if len(submission.Status.Results) > 0 {
		return ctrl.Result{}, nil
	}

	suite, exists := submission.ObjectMeta.Labels[labelSuiteKey]
	if !exists {
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: No label"))
		// TODO: ERROR?
		return ctrl.Result{}, nil
	}

	listOpts := client.ListOptions{}

	client.MatchingLabels{
		labelSuiteKey: suite,
	}.ApplyToList(&listOpts)

	tcs := distrustv1alpha1.TestCaseList{}
	if err := r.List(ctx, &tcs, &listOpts); err != nil {
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
		return ctrl.Result{}, err
	}

	logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %d test cases", len(tcs.Items)))

	if len(tcs.Items) == 0 {
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: No test cases..."))
		return ctrl.Result{
			RequeueAfter: time.Minute,
		}, nil
	}

	submissionID := fmt.Sprintf("%s-%d", submission.ObjectMeta.UID, submission.ObjectMeta.Generation)
	ls := metav1.LabelSelector{
		MatchLabels: map[string]string{
			labelSubmissionKey: submissionID,
		},
	}

	ss := appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: submission.Namespace,
			Labels: map[string]string{
				labelProject: submission.Spec.UserName + "-" + submission.Name,
			},
			GenerateName: submission.Spec.UserName + "-" + submission.Name + "-",
			OwnerReferences: []metav1.OwnerReference{
				{
					Kind:       submission.Kind,
					APIVersion: submission.APIVersion,
					UID:        submission.UID,
					Name:       submission.Name,
				},
			},
		},
		Spec: appsv1.StatefulSetSpec{
			Selector: &ls,
			Template: v1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: map[string]string{
						labelSubmissionKey: submissionID,
					},
				},
				Spec: v1.PodSpec{
					Containers: []v1.Container{
						{
							Name:            "server",
							Image:           submission.Spec.ServerImage,
							ImagePullPolicy: v1.PullAlways,
						},
					},
				},
			},
		},
	}

	if err := r.Create(ctx, &ss); err != nil {
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
		return ctrl.Result{}, err
	}

	svc := v1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Namespace:    submission.Namespace,
			GenerateName: submission.Spec.UserName + "-" + submission.Name + "-",
			Labels: map[string]string{
				labelProject: submission.Spec.UserName + "-" + submission.Name,
			},
		},
		Spec: v1.ServiceSpec{
			Selector: map[string]string{
				labelSubmissionKey: submissionID,
			},
			Ports: []v1.ServicePort{
				{
					Name: "server-port",
					Port: submission.Spec.ServerPort,
				},
			},
		},
	}

	if err := r.Create(ctx, &svc); err != nil {
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
		r.Delete(ctx, &ss)
		return ctrl.Result{}, err
	}

	for ss.Status.ReadyReplicas < 1 {
		r.Get(ctx, types.NamespacedName{
			Namespace: ss.Namespace,
			Name:      ss.Name,
		}, &ss)

		time.Sleep(time.Second)
	}

	start := metav1.Now()
	for _, tc := range tcs.Items {
		jobs := make([]batchv1.Job, len(tc.Spec.ClientInput))

		// TODO: Create in stopped state, to start all at once
		for k, c := range tc.Spec.ClientInput {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: found tc %s-%s-%d", tc.Labels[labelSuiteKey], tc.Spec.CaseID, k))

			j := batchv1.Job{
				ObjectMeta: metav1.ObjectMeta{
					Namespace: submission.Namespace,
					Annotations: map[string]string{
						labelConnects: fmt.Sprintf(`[{"apiVersion":"apps/v1","kind":"StatefulSet","name":"%s"}]`, ss.Name),
					},
					Labels: map[string]string{
						labelProject: submission.Spec.UserName + "-" + submission.Name,
					},
					GenerateName: submission.Spec.UserName + "-" + submission.Name + "-",
					OwnerReferences: []metav1.OwnerReference{
						{
							Kind:       submission.Kind,
							APIVersion: submission.APIVersion,
							UID:        submission.UID,
							Name:       submission.Name,
						},
					},
				},
				Spec: batchv1.JobSpec{
					Template: v1.PodTemplateSpec{
						Spec: v1.PodSpec{
							RestartPolicy: v1.RestartPolicyNever,
							Containers: []v1.Container{
								{
									Name:            "client",
									Image:           submission.Spec.ClientImage,
									ImagePullPolicy: v1.PullAlways,
									Args:            c.Inputs,
									Env: []v1.EnvVar{
										{
											Name:  "SERVER_HOST",
											Value: svc.Name,
										},
										{
											Name:  "SERVER_PORT",
											Value: fmt.Sprint(svc.Spec.Ports[0].Port),
										},
									},
								},
							},
						},
					},
				},
			}

			if err := r.Create(ctx, &j); err != nil {
				logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
				r.Delete(ctx, &svc)
				r.Delete(ctx, &ss)
				return ctrl.Result{}, err
			}

			jobs[k] = j
		}

		// wait for jobs
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: Jobs: %d", len(jobs)))
		for _, j := range jobs {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: Job: %+v", j))
			wait := true
			for wait {
				for _, cond := range j.Status.Conditions {
					logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: Cond: %+v", cond))
					if cond.Type == batchv1.JobComplete {
						wait = cond.Status != "True"
					}
				}

				r.Get(ctx, types.NamespacedName{
					Namespace: j.Namespace,
					Name:      j.Name,
				}, &j)

				time.Sleep(time.Second)
			}
		}

		r.Get(ctx, types.NamespacedName{
			Namespace: ss.Namespace,
			Name:      ss.Name,
		}, &ss)

		// They've come too far to make it out alive
		bgdel := metav1.DeletePropagationBackground
		delOpts := client.DeleteOptions{
			PropagationPolicy: &bgdel,
		}
		defer func() {
			for _, j := range jobs {
				r.Delete(ctx, &j, &delOpts)
			}
		}()
		defer r.Delete(ctx, &svc)
		defer r.Delete(ctx, &ss)

		// @REF [Get pod logs](https://stackoverflow.com/questions/53852530/how-to-get-logs-from-kubernetes-using-go/53870271#53870271)
		serverPod := v1.Pod{}
		if err := r.Get(ctx, types.NamespacedName{
			Namespace: ss.Namespace,
			Name:      ss.Name + "-0",
		}, &serverPod); err != nil {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: CANT FIND POD: %v", err))
			return ctrl.Result{}, err
		}

		// This fails unless you're in a cluster
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: Before config: %+v", serverPod))
		config, err := rest.InClusterConfig()
		if err != nil {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
			return ctrl.Result{}, err
		}
		logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: Got config"))

		clientset, err := kubernetes.NewForConfig(config)
		if err != nil {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
			return ctrl.Result{}, err
		}

		podLogsOpts := v1.PodLogOptions{
			SinceTime: &start,
		}
		podReq := clientset.CoreV1().Pods(serverPod.Namespace).GetLogs(serverPod.Name, &podLogsOpts)
		podLogs, err := podReq.Stream(ctx)
		if err != nil {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
			return ctrl.Result{}, err
		}
		defer podLogs.Close()

		buf, err := io.ReadAll(podLogs)
		if err != nil {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
			return ctrl.Result{}, err
		}
		logs := string(buf)

		if logs == tc.Spec.ServerOutput[0].Outputs[0] {
			submission.Status.Results = append(submission.Status.Results, distrustv1alpha1.Result{
				Case:   tc.Spec.CaseID,
				Status: "passed",
			})
		} else {
			r := distrustv1alpha1.Result{
				Case:   tc.Spec.CaseID,
				Status: "failed",
			}
			if tc.Spec.Visible {
				r.Diff = cmp.Diff(tc.Spec.ServerOutput[0].Outputs[0], logs)
			}
			submission.Status.Results = append(submission.Status.Results, r)
		}

		if err := r.Status().Update(ctx, &submission); err != nil {
			logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: %v", err))
			return ctrl.Result{}, err
		}

	}

	logger.Info(fmt.Sprintf("SUBMISSION RECONCILING: Done!"))

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *SubmissionReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&distrustv1alpha1.Submission{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&batchv1.Job{}).
		Watches(&distrustv1alpha1.TestCase{}, &handler.Funcs{}).
		Complete(r)
}
